package swen90006.machine;

public class SimpleTest {

  private static final byte[] MINUS_ONE = {'0','1','s'};
  private static final byte[] TEN = {'5','2','m'};
  private static final byte[] SEVENTEEN = {'5','4','a','7','s','9','m','1','s','1','d'};

  public static void main(String[] args) throws Exception
  {
    Machine m = new Machine(100);
    Integer res = null;
    if (m.execute(MINUS_ONE) != -1){
      throw new Exception("Not MINUS_ONE");
    }

    if (m.execute(TEN) != 10){
      throw new Exception("Not TEN");
    }

    if (m.execute(SEVENTEEN) != 17){
      throw new Exception("Not SEVENTEEN");
    }
  }
}
