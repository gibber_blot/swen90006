package swen90006.machine;

public class ShortStackException extends RuntimeException
{
  //The attempted instruction
  private byte instruction_;

  public ShortStackException(byte instruction)
  {
    instruction_ = instruction;
  }

  public String toString()
  {
    return "ShortStackException on instruction " + instruction_;
  }
}
