package swen90006.machine;

public class InvalidOffsetException extends RuntimeException
{
  //The attempted instruction
  private byte instruction_;

  //The attempted offset
  private int offset_;

  public InvalidOffsetException(byte instruction, int offset)
  {
    instruction_ = instruction;
    offset_ = offset;
  }

  public String toString()
  {
    return "InvalidOffsetException on instruction " + instruction_ + 
      "; offset: " + offset_;
  }
}
