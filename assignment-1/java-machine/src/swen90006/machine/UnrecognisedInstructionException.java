package swen90006.machine;

public class UnrecognisedInstructionException extends RuntimeException
{
  //The character that is unrecognised
  private int val_;

  public UnrecognisedInstructionException(int val)
  {
    val_ = val;
  }

  public String toString()
  {
    return "UnrecognisedInstructionException: " + val_;
  }
}
