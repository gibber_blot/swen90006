package swen90006.machine;

public class DivideByZeroException extends ArithmeticException
{
  public DivideByZeroException() {}

  public String toString()
  {
    return "DivideByZeroException";
  }
}
