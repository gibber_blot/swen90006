package swen90006.machine;

public class FullStackException extends RuntimeException
{
  //The value that was attempted to be pushed
  private int val_;

  public FullStackException(int val)
  {
    val_ = val;
  }

  public String toString()
  {
    return "FullStackException: " + val_;
  }
}
