package swen90006.machine;

public class Machine 
{
  public static final byte INSTRUCTION_ADD = 'a';
  public static final byte INSTRUCTION_SUBTRACT = 's';
  public static final byte INSTRUCTION_READ = 'r';
  public static final byte INSTRUCTION_MULT = 'm';
  public static final byte INSTRUCTION_DIVIDE = 'd';
  public static final byte INSTRUCTION_POP = 'p';
  public static final byte INSTRUCTION_WRITE = 'w';

  private int[] stack;
  private int sp; /** stack pointer (an index into stack). 
		      Invariant: 0 <= sp <= stack.length - 1 */

  public Machine(int stacklen)
  {
    stack = new int[stacklen];
    sp = 0;
  }

  private void do_add() throws ShortStackException
  {
    if (sp < 2){
      throw new ShortStackException(INSTRUCTION_ADD);
    }
    int a = stack[sp - 2];
    int b = stack[sp - 1];
    stack[sp - 2] = a + b;
    sp--;
  }

  private void do_subtract() throws ShortStackException
  {
    if (sp < 2){
      throw new ShortStackException(INSTRUCTION_SUBTRACT);
    }
    int a = stack[sp - 2];
    int b = stack[sp - 1];
    stack[sp - 2] = a - b;
    sp--;
  }
    
  private void do_mult() throws ShortStackException
  {
    if (sp < 2){
      throw new ShortStackException(INSTRUCTION_MULT);
    }
    int a = stack[sp - 2];
    int b = stack[sp - 1];
    stack[sp - 2] = a * b;
    sp--;
  }

  private void do_divide() throws ShortStackException, DivideByZeroException
  {
    if (sp < 2){
      throw new ShortStackException(INSTRUCTION_DIVIDE);
    }
    int a = stack[sp - 2];
    int b = stack[sp - 1];
	
    if (b == 0){
      throw new DivideByZeroException();
    }

    stack[sp - 2] = a / b;
    sp--;
  }

  private void do_pop() throws ShortStackException
  {
    if (sp < 1){
      throw new ShortStackException(INSTRUCTION_POP);
    }
    //Mutant_4: Do not pop stack
    //sp--;
  }

  private void do_read() throws ShortStackException, InvalidOffsetException
  {
    if (sp < 1){
      throw new ShortStackException(INSTRUCTION_READ);
    }
    int offset = stack[sp - 1];

    /* Check offset */
    if (offset < 0 || offset > sp - 1){
      throw new InvalidOffsetException(INSTRUCTION_READ, offset);
    }

    stack[sp - 1] = stack[sp - 1 - offset];
  }

  private void do_write() throws ShortStackException, InvalidOffsetException
  {
    if (sp < 2){
      throw new ShortStackException(INSTRUCTION_WRITE);
    }
    int offset = stack[sp - 2];
    int val = stack[sp - 1];

    if (offset < 0 || offset > sp){
      throw new InvalidOffsetException(INSTRUCTION_WRITE, offset);
    }

    stack[sp - 1 - offset] = val;
  }

  private void do_push(int val) throws FullStackException
  {
    if (sp >= stack.length - 1){
      throw new FullStackException(val);
    }

    stack[sp] = val;
    sp++;
  }

  /** Execute a sequence of machine instructions.
   *
   * @param instructions is an array containing the instructions (each of
   *        which is a single byte).
   * @return the element on the top of the stack at the end of executing
   *         the instructions.
   * @throws Exception when instructions array has unrecognised or 
   *         invalid instructions, or when it leaves no result on the top
   *         of the stack when it finishes
   */
  int execute(byte[] instructions) 
    throws FullStackException,
	   ShortStackException,
	   InvalidOffsetException,
	   NoReturnValueException,
	   UnrecognisedInstructionException
  {
    sp = 0; /* clear the stack */
    for (int i = 0; i < instructions.length; i++){
      switch (instructions[i]){
		
      case INSTRUCTION_ADD:
	do_add();
	break;
      case INSTRUCTION_SUBTRACT:
	do_subtract();
	break;
      case INSTRUCTION_MULT:
	do_mult();
	break;
      case INSTRUCTION_DIVIDE:
	do_divide();
	break;
      case INSTRUCTION_READ:
	do_read();
	break;
      case INSTRUCTION_WRITE:
	do_write();
	break;
      case INSTRUCTION_POP:
	do_pop();
	break;
      default:
		
	if (instructions[i] >= '0' && instructions[i] <= '9'){
	  do_push(instructions[i] - '0');
	}else{
	  throw new UnrecognisedInstructionException(instructions[i]);
	}
      }
    }
    /* get the program result to return */
    if (sp < 1){
      throw new NoReturnValueException();
    }

    /* program executed successfully */
    return stack[sp - 1];
  }
}
